const mongoose = require('mongoose');

const {
    Boards,
} = require('../data');


const add = async (boardId, name) => {

    let board = await Boards.findById(boardId)

    board.lists.push({name: name, cards: []})
    return await board.save();
};

const getAll = async (boardId) => {

    let board = await Boards.findById(boardId)
    return board ? board.lists : [];
};


const getById = async (id) => {
    
    let boards = await Boards.find({'lists._id' : mongoose.Types.ObjectId(id)})
    return boards.length > 0 ? boards[0].lists.find(list => list._id == id) : {}
};

const updateById = async (id, name) => {

    return await Boards.updateOne(
        {'lists._id' : mongoose.Types.ObjectId(id)},
        { $set: {'lists.$.name': name} }
    )
};

const deleteById = async (id) => {
    return await Boards.update(
        {'lists._id' : mongoose.Types.ObjectId(id)},
        { $pull: {'lists':  { '_id': mongoose.Types.ObjectId(id)}} }
    )
};

module.exports = {
    add,
    getAll,
    getById,
    updateById,
    deleteById
}