const Router = require('express')();

const BoardsController = require('../Boards/controllers.js');
const ListsController = require('../Lists/controllers.js');
const CardsController = require('../Cards/controllers.js');


Router.use('/boards', BoardsController);
Router.use('/lists', ListsController);
Router.use('/cards', CardsController);


module.exports = Router;