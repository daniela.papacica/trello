const {
    Boards,
} = require('../data');


const add = async (name) => {
    const board = new Boards({
        name: name
    });
    return await board.save();
};

const getAll = async () => {
    return await Boards.find()
};


const getById = async (id) => {
    return await Boards.findById(id)
};

const updateById = async (id, name) => {
    await Boards.findByIdAndUpdate(id, {name});
};

const deleteById = async (id) => {
    await Boards.findByIdAndDelete(id);
};

module.exports = {
    add,
    getAll,
    getById,
    updateById,
    deleteById
}