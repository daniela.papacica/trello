import Home from './components/Home.vue';
import BoardPage from './components/BoardPage.vue';
import VueRouter from 'vue-router'

let routes = [
  {
    path: '/',
    name : 'home',
    component: Home
  },
  {
    path: '/boards/:id',
    name : 'boardpage',
    component: BoardPage
  }
];

export default new VueRouter({
 routes: routes, mode : 'history'
})