const mongoose = require('mongoose');
require('dotenv').config();

(async () => {
  try {

    await mongoose.connect(`mongodb://${process.env.MHOST}:${process.env.MPORT}/${process.env.MDATABASE}`,
      {
        useFindAndModify: false,
        useNewUrlParser: true,
        useUnifiedTopology: true
      }
    );

  } catch (e) {
    console.trace(e);
  }
})();

const Boards = require('./models/Boards.js');

module.exports = {
  Boards,
}