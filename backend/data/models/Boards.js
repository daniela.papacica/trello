const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const CardsSchema = new Schema({
    title: {
        type: String,
        required: false
    },
    description: {
        type: String,
        required: false
    },
    
}, { timestamps: true });

const ListsSchema = new Schema({
    cards: [CardsSchema],
    name: {
        type: String,
        required: true
    },
    
}, { timestamps: true });

const BoardsSchema = new Schema({
    lists: [ListsSchema],
    name: {
        type: String,
        required: true
    },
    
}, { timestamps: true });

const BoardsModel = mongoose.model('boards', BoardsSchema);

module.exports = BoardsModel;
