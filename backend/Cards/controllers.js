const express = require('express');

const CardsService = require('./services.js');

const {
    ServerError
} = require('../errors');

const router = express.Router();


// --------------- Card


router.get('/:id', async (req, res, next) => {
    const {
        id
    } = req.params;

    try {
        const card = await CardsService.getById(id);
        res.json(card)
    } catch (err) {
        next(err);
    }
});


router.get('/board/:boardId', async (req, res, next) => {
    const {
        boardId
    } = req.params;

    try {
        const cards = await CardsService.getAllFromBoard(boardId);
        res.json(cards)
    } catch (err) {
        next(err);
    }
});

router.get('/list/:listId', async (req, res, next) => {
    const {
        listId
    } = req.params;

    try {
        const cards = await CardsService.getAllFromList(listId);
        res.json(cards)
    } catch (err) {
        next(err);
    }
});


router.post('/:listId', async (req, res, next) => {
    const {
        listId
    } = req.params;

    const {
        title, description
    } = req.body;

    try {
        let card = await CardsService.add(listId, title, description);
        res.json(card)
    } catch (err) {
        next(err);
    }
});


router.put('/:id', async (req, res, next) => {
    const {
        id
    } = req.params;

    const {
        title, description
    } = req.body;


    try {
        let card = await CardsService.updateById(id, title, description);
        res.json(card)
    } catch (err) {
        next(err);
    }
});


router.delete('/:id', async (req, res, next) => {
    const {
        id
    } = req.params;


    try {
        
        await CardsService.deleteById(id);
        res.status(204).end();

    } catch (err) {
        next(err);
    }
});

module.exports = router;