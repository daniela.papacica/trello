const mongoose = require('mongoose');
const {
    Boards,
} = require('../data');


const add = async (listId, title, description) => {
    return await Boards.updateOne(
        {'lists._id' : mongoose.Types.ObjectId(listId)},
        { $push: {'lists.$.cards': {'title': title, 'description': description}} }
    )
};


const getById = async (id) => {

    let boards =  await Boards.aggregate([
        { $project: {
            _id: 1,
            lists: 1
        } },
        {$unwind: "$lists"},
        {$unwind: '$lists.cards' },
    ])

    let card = boards.map(board => board.lists.cards).find(card => card._id == id)

    return card

};


const getAllFromBoard = async (boardId) => {
    let boards =  await Boards.aggregate([
        { $project: {
            _id: 1,
            lists: 1
        } },
        {$unwind: "$lists"},
        {$unwind: '$lists.cards' },
    ])

    return boards.filter(board => board._id == boardId).map(board => board.lists.cards)
};

const getAllFromList = async (listId) => {
    let boards =  await Boards.aggregate([
        { $project: {
            _id: 1,
            lists: 1,
        } },
        {$unwind: "$lists"},
        {$unwind: '$lists.cards' },
    ])

    return boards.filter(board => board.lists._id == listId).map(board => board.lists.cards)
};


const updateById = async (id, title, description) => {
    let boards =  await Boards.aggregate([
        { $project: {
            _id: 1,
            lists: 1
        } },
        {$unwind: "$lists"},
        {$unwind: '$lists.cards' },
    ])

    let key = boards.find(board => board.lists.cards._id == id)
    if(key){
        let board = await Boards.findById(key._id);
        let listIndex = board.lists.findIndex(Element => JSON.stringify(Element._id) == JSON.stringify(key.lists._id))
        let cardIndex = board.lists[listIndex].cards.findIndex(Element => Element._id == id)

        board.lists[listIndex].cards[cardIndex].title = title
        board.lists[listIndex].cards[cardIndex].description = description

        board.save()
    }
    else return

};

const deleteById = async (id) => {
    let boards =  await Boards.aggregate([
        { $project: {
            _id: 1,
            lists: 1
        } },
        {$unwind: "$lists"},
        {$unwind: '$lists.cards' },
    ])

    let key = boards.find(board => board.lists.cards._id == id)
    if(key){
        let board = await Boards.findById(key._id);
        let listIndex = board.lists.findIndex(Element => JSON.stringify(Element._id) == JSON.stringify(key.lists._id))
        let cardIndex = board.lists[listIndex].cards.findIndex(Element => Element._id == id)

        board.lists[listIndex].cards.splice(cardIndex, 1)

        board.save()
    }
    else return
};

module.exports = {
    add,
    getAllFromBoard,
    getAllFromList,
    getById,
    updateById,
    deleteById
}