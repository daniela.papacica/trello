const express = require('express');

const ListsService = require('./services.js');

const {
    ServerError
} = require('../errors');

const router = express.Router();


// --------------- List

router.get('/:id', async (req, res, next) => {
    const {
        id
    } = req.params;

    try {
        const list = await ListsService.getById(id);
        res.json(list)
    } catch (err) {
        next(err);
    }
});


router.get('/board/:boardId', async (req, res, next) => {
    const {
        boardId
    } = req.params;

    try {
        const lists = await ListsService.getAll(boardId);
        res.json(lists)
    } catch (err) {
        next(err);
    }
});


router.post('/:boardId', async (req, res, next) => {
    const {
        boardId
    } = req.params;

    const {
        name
    } = req.body;

    try {
        let list = await ListsService.add(boardId, name);
        res.json(list)

    } catch (err) {
        next(err);
    }
});


router.put('/:id', async (req, res, next) => {
    const {
        id
    } = req.params;

    const {
        name
    } = req.body;


    try {
        let list = await ListsService.updateById(id, name);
        res.json(list)
    } catch (err) {
        next(err);
    }
});


router.delete('/:id', async (req, res, next) => {
    const {
        id
    } = req.params;


    try {
        
        await ListsService.deleteById(id);
        res.status(204).end();

    } catch (err) {
        next(err);
    }
});

module.exports = router;