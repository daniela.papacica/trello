const express = require('express');

const BoardsService = require('./services.js');

const {
    ServerError
} = require('../errors');

const router = express.Router();



// --------------- Board
router.get('/', async (req, res, next) => {
    try {
        const boards = await BoardsService.getAll();
        res.json(boards)
    } catch (err) {
        next(err);
    }
});

router.get('/:id', async (req, res, next) => {
    const {
        id
    } = req.params;

    try {
        const board = await BoardsService.getById(id);
        res.json(board)
    } catch (err) {
        next(err);
    }
});


router.post('/', async (req, res, next) => {
    const {
        name
    } = req.body;

    try {
        let board = await BoardsService.add(name);
        res.json(board);

    } catch (err) {
        next(err);
    }
});


router.put('/:id', async (req, res, next) => {
    const {
        id
    } = req.params;

    const {
        name
    } = req.body;

    try {
        let board = await BoardsService.updateById(id, name);
        res.json(board)
    } catch (err) {
        next(err);
    }
});


router.delete('/:id', async (req, res, next) => {
    const {
        id
    } = req.params;


    try {
        
        await BoardsService.deleteById(id);
        res.status(204).end();

    } catch (err) {
        next(err);
    }
});


module.exports = router;